# FBI Crimes Llama 🐪 Project

The aim of this project is to find out whether crimes in United States are affected by other factors such as unemployment and income.
After ascertaining that there exists a relation between crime rate and the other datasets, we forecast the criminalisation in the USA states.


## Getting Started

In the requirements.txt file there are all the packets needed for the project.

```
pip install -r requirements.txt
``` 
All the project steps can be found [here](https://docs.google.com/document/d/1MiIT43zJKpSllqrWKclBuMF0gQ5Yz_CaZvTdxx9hZkM/edit?usp=sharing)

## Datasets
We use as keys state and year
* **FBI for crimes.** 
This dataset provides us information about:
   * State
   * Year 
   * Population
   * Total crimes(also divided by type)
* **US unemployment rate by country.**
This dataset provides us information about:
   * Year
   * Month
   * State 
   * County
   * Rate 
* **US census bureau for income.**
This dataset provides us information about: 
   * Year 
   * State 
   * Median income
   

## Authors
* **Enrico Bagattin**
* **Lucrezia Wally Bestetti** 
* **Francesco Di Vincenzo**
* **Virginia Massaccesi** 
* **Francesca Michielan**

## Personal Challenge - Bagattin Enrico 
As a personal challenge I will display the cities with the highest crime rate in the world. 
I start scraping all the data I need from the Numbeo website using the Beautiful Soup python library.
Then I will add the cities' coordinates (latitude and longitude) using a public api (OpenCage Geocoder).
Finally I modified a Google open project to show all the data in a live globe built with js; for this final part I've to thanks Dario Scalabrin for the collaboration to improve both projects.
The final result can be run locally or found [there](https://crimes-worldwide.herokuapp.com).

* [Numbeo](https://www.numbeo.com/crime/rankings.jsp): Data source
* [Beautiful Soup](https://www.crummy.com/software/BeautifulSoup/bs4/doc/): Web-scraping library
* [OpenCage Geocoder](https://opencagedata.com/): API for city coordinates given a string (2500 requests limit)
* [Heroku](www.heroku.com): A cloud platform for deploying web apps
* [WebGL Globe](https://experiments.withgoogle.com/chrome/globe): An open platform for geographic data visualization

To run the project locally install Node.js and run:
```
npm install http-server -g
``` 
In the webgl-globe-master folder of the project:
``` 
http-server .
``` 
## Credits
Special thanks to:
* [FBI](www.fbi.gov) Crimes Datasets
* [Kaggle](www.kaggle.com) Website