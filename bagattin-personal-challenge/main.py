# Bagattin Enrico - Personal Challenge

# As a personal challenge I decided to display through an interactive 3D-map the cities with the highest crime rate in
# the world. As first step, I started with the data scraping which were available in the Numbeo website and I used the
# Beautiful Soup python as library. Then I added the cities' coordinates (latitude and longitude) using a public API
# (OpenCage Geocoder). Finally I modified a Google open project to show all the data in a live globe built with js;
# for this final part I've to thanks Dario Scalabrin for the collaboration to improve both projects.
# The final result can be ran locally or found here: crimes-worldwide.herokuapp.com

import pandas as pd
import os
import json
import requests
from scrape_numbeo import ScrapeNumbeo

DATASETS_DIRECTORY = os.path.join('..', 'datasets')
KEY = '2326dbb5bba84bbc8dd852664c594df7'  # This key is used for OpenCage Geocoder APIs

# Scrape the data from the Numbeo website
path = os.path.join(DATASETS_DIRECTORY, 'cities_crimes', 'scraped.csv')
if not os.path.exists(path):
    scrape_world_crimes = ScrapeNumbeo("https://www.numbeo.com/crime/rankings.jsp")
    scraped_dataframe = scrape_world_crimes.to_dataframe()
    scraped_dataframe.to_csv(path, index=True)
scraped_dataframe = pd.read_csv(path, index_col=0)

# Load all cities coordinates from opencagedata api
path = os.path.join(DATASETS_DIRECTORY, 'cities_crimes', 'cities.csv')
if not os.path.exists(path):
    cities = scraped_dataframe.City.drop_duplicates()
    for city in cities:
        data = {
                    'q': city,
                    'key': KEY,
                    'no_annotations': 1,
                    'limit': 1
                }

        response = requests.get('https://api.opencagedata.com/geocode/v1/json', params=data)

        if response.status_code == 200:
            response_json = response.json()
            if 'results' in response_json and len(response_json['results']) > 0:
                geometry = response_json['results'][0]['geometry']
                components = response_json['results'][0]['components']
                if 'lat' in geometry:
                    scraped_dataframe.loc[scraped_dataframe.City == city, 'Lat'] = geometry['lat']
                if 'lng' in geometry:
                    scraped_dataframe.loc[scraped_dataframe.City == city, 'Lng'] = geometry['lng']
                if 'continent' in components:
                    scraped_dataframe.loc[scraped_dataframe.City == city, 'Continent'] = components['continent']
                if 'country' in components:
                    scraped_dataframe.loc[scraped_dataframe.City == city, 'Country'] = components['country']
                if 'country_code' in components:
                    scraped_dataframe.loc[scraped_dataframe.City == city, 'Code'] = components['country_code']

    scraped_dataframe.dropna(how='all', inplace=True)
    scraped_dataframe.to_csv(path, index=True)
cities_dataframe = pd.read_csv(path, index_col=0)

# Export the ready-to-use json for the WebGL project
final_json = []
for year in [2019, 2017, 2015]:
    data_list = []
    data = cities_dataframe[cities_dataframe.Year == year]
    for index, row in data.iterrows():
        data_list += [row['Lat']]
        data_list += [row['Lng']]
        data_list += [row['Crime Index']/250]
    final_json += [[str(year), data_list]]
with open(os.path.join('..', 'webgl-globe-master', 'cities.json'), 'w') as f:
    json.dump(final_json, f)
