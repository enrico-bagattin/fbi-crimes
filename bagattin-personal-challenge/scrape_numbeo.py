import requests
from bs4 import BeautifulSoup
import pandas as pd


def get_page(url):
    """
      Gets the page from the url
      Args:
        url: String
      Return:
        A request that contains the html page
    """
    request = requests.get(url)
    if request.status_code != 200:
        return None
    return request


def extract_table(html, year):
    """
      A method to extract from an html the table contents and store them into a dictionary
      Args:
        html: String
        year: Integer
      Return:
        A pandas Dataframe with the table contents
    """
    table = html.find("table", {'id': 't2'})

    columns = []
    table_head = table.find_all('th')
    for th in table_head:
        columns.append(th.div.text)

    lines = []
    table_rows = table.find_all('tr')
    for tr in table_rows:
        td = tr.find_all('td')
        if len(td) != 0:
            row = [x.text for x in td]
            lines.append(row)
    data = pd.DataFrame(lines, columns=columns)
    data['Year'] = year
    return data


class ScrapeNumbeo:
    """
    This class is used to scrape Numbeo website tables
    """
    # It's tested to work with "https://www.numbeo.com/crime/rankings.jsp"

    def __init__(self, url):
        self.url = url
        response = get_page(self.url)
        if response:
            # Take the response string an parse it into an html page
            page = BeautifulSoup(response.text, features="html.parser")
            years_form = page.find("form", {"class": "changePageForm"})
            # Extract values from the page dropdown and filter them removing the mid term values ('2018-mid')
            self.years = [values["value"] for values in years_form("option") if not values['value'].endswith('-mid')]

    def to_dataframe(self):
        """
          A method to export all the url tables into a dataframe
          Return:
            A pandas Dataframe with the tables contents
        """
        result_frame = None
        for y in self.years:
            response = get_page(self.url + '?title=' + y)  # Take the html page of each year
            if response:
                page = BeautifulSoup(response.text, features="html.parser")
                year_dataframe = extract_table(page, int(y[:4]))
                if result_frame is None:
                    result_frame = year_dataframe
                else:
                    result_frame = result_frame.append(year_dataframe, ignore_index=True)
        if result_frame is not None:
            result_frame.drop('Rank', inplace=True, axis=1)  # Drop the Rank empty column (only for Numbeo crime tables)
        return result_frame  # Return the resulting dataframe after all the years are being appended
