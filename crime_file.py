import os
import pandas as pd


class CrimeFile:
    """
      This class is used to define the crime files
    """
    DATASETS_DIRECTORY = 'datasets'
    PATH = os.path.join(DATASETS_DIRECTORY, 'fbi-crimes')

    def __init__(self, year, n_rows, raw_filename):
        self.year = year
        self.n_rows = n_rows
        self.raw_filename = raw_filename
        self.filename = 'crimes_' + raw_filename

    def generate_simplified(self):
        """
          The function create a new xls file with the useful data of the chosen crime file
          Args:
            self: A crime file object
          Return:
            The filename of the generated file
        """
        path = os.path.join(self.PATH, self.filename)
        if not os.path.exists(path):
            # -- Import the file --
            xls = pd.ExcelFile(os.path.join(self.PATH, self.raw_filename))  # Import of the crimes data-set
            crime = pd.read_excel(xls, header=3, index_col=0,
                                  nrows=self.n_rows)  # Read an Excel file into a pandas DataFrame

            # -- Adjusting Rows --
            # Remove null elements, leading and training whitespaces, numbers and commas from the index
            new_index = crime.index.dropna().str.replace(r'[0-9]+', '').str.replace(',', '').str.strip()
            # Remove the previous year (es. 2013) and 'Percent change' rows from the DataFrame
            crime = crime[crime.Year == self.year]
            crime.index = new_index

            # -- Adjusting Columns --
            # Remove numbers and line feeds (\n) from the column titles
            new_columns = crime.columns.str.replace(r'[0-9]+', '').str.replace('\n', ' ').str.replace('  ', ' ')
            crime.columns = new_columns
            crime.drop('Unnamed: ', axis=1, inplace=True)  # Remove the unnamed columns

            # -- Save the new file --
            crime.to_excel(path, sheet_name='Crimes', index=True)

        return path
