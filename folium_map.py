import folium
import json
import random

# Markers for the interactive map
MARKERS = [
    {'location': 'Alabama', 'coordinates': [33, -87], 'popup': 'In this State the crime rate is on average higher.than '
                                                               'others. There exist a correlation in particular with '
                                                               'the variable income.', 'icon_color': 'red'},
    {'location': 'Arkansas', 'coordinates': [34, -93], 'popup': 'In this State the crime rate is on average higher than'
                                                                ' others. There exist a correlation in particular with '
                                                                'the variable income.', 'icon_color': 'red'},
    {'location': 'Arizona', 'coordinates': [33, -112], 'popup': 'In this State the crime rate is on average higherthan '
                                                                'others. There is no correlation in with the variable '
                                                                'income but we can state that the rate of unemployement'
                                                                ' in consistent in Arizona.', 'icon_color': 'red'},
    {'location': 'Louisiana', 'coordinates': [30,-93], 'popup': 'In this State the crime rate is on average higher than'
                                                                ' others. There exist a correlation in particular with '
                                                                'the variable income but also with the other variable '
                                                                'rate of unemployement', 'icon_color': 'red'},
    {'location': 'South Karolina', 'coordinates': [33,-81], 'popup': 'In this State the crime rate is on average '
                                                                     'higher than others. We have not notice a '
                                                                     'correlation both with the variable "income" '
                                                                     'but also with the rate of unemployement which'
                                                                     ' is on everage higher.', 'icon_color': 'red'},
    {'location': 'Tennesse', 'coordinates': [36,-87], 'popup': 'In this State the crime rate is on average higher than '
                                                               'others. There exist a correlation not only, with the '
                                                               'variable income (lower than the other US States) but '
                                                               'also with the other variable (rate of unemployement)',
     'icon_color': 'red'},
    {'location': 'Washington', 'coordinates': [47,-122], 'popup': 'In this State the crime rate is on average higher '
                                                                  'than others. We have not notice a correlation with '
                                                                  'the variable "income" but we can state that the rate'
                                                                  ' of unemployement is on everage higher.',
     'icon_color': 'red'},
    {'location': 'New Mexico', 'coordinates': [34,-106], 'popup': 'In this State the crime rate is on average higher '
                                                                  'than the other States. In New Mexico, according to '
                                                                  'the data in our possesion, the crime rate is higher'
                                                                  ' than teh one of any other US State. There exist a '
                                                                  'correlation with the variable income (solid) and '
                                                                  'with the rate of unemployement', 'icon_color': 'red'}
]
COLOR_SCHEMES = ['YlGn', 'BuPu', 'OrRd', 'BuGn', 'GnBu', 'PuBu', 'PuRd', 'RdPu', 'PiYG']


class FoliumMap:
    """
      This class is used to create an interactive map with the Folium library
    """
    
    def __init__(self, dataset, location, states_json_file):
        self.states_json = states_json_file
        state_geo_json = json.loads(open(self.states_json, "r").read())
        us_states = [x['properties']['name'] for x in state_geo_json['features']]
        self.dataset = dataset[dataset.State.isin(us_states)].copy()
        self.location = location
        self.map = folium.Map(location=location, zoom_start=3)

    def clear_map(self):
        """
          Clean the map layers and markers
        """
        self.map = folium.Map(location=self.location, zoom_start=3)

    def add_marker(self, coordinates, popup, icon_color):
        """
          Add a marker to the map
          Args:
            coordinates: A list of coordinates
            popup: The popup text
            icon_color: The marker color
        """
        folium.Marker(location=coordinates, popup=popup,
                      icon=folium.Icon(color=icon_color)).add_to(self.map)

    def add_layer(self, column, color, data):
        """
          Add a layer to the map
          Args:
            column: The dataset's column to be used
            color: The layer color scheme
            data: The dataset to use
        """
        folium.Choropleth(
            geo_data=self.states_json,
            name=column,
            data=data,
            columns=['State', column],
            key_on='feature.properties.name',
            fill_color=color,
            fill_opacity=0.7,
            line_opacity=0.2,
            legend_name=column
        ).add_to(self.map)

    def generate_map(self, columns, year=None, with_markers=False):
        """
          Generates a Folium map with the given characteristics
          Args:
            columns: The list of dataset's column to be used
            year: The year to use (None for the mean of all the years)
            with_markers: Boolean value to choose the presence of markers
          Return:
            The generated map
        """

        self.clear_map()

        # Take all the years' data (the mean) or only a slice (with the given year)
        if year is None:
            dataset_slice = self.dataset.groupby('State').mean().reset_index()
        else:
            dataset_slice = self.dataset[self.dataset.Year == year]

        # Add the map markers
        if with_markers:
            for marker in MARKERS:
                self.add_marker(marker['coordinates'], marker['location']+': '+marker['popup'], marker['icon_color'])

        # Add all the map layers (with a random color if it's only one)
        n = 0
        if len(columns) == 1:
            n = random.randint(0, 8)
        for i, col in enumerate(columns[:10]):
            self.add_layer(col, COLOR_SCHEMES[i+n], dataset_slice)

        # Add the layer control
        folium.LayerControl().add_to(self.map)

        return self.map
