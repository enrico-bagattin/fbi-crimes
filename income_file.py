import os
import pandas as pd

DATASETS_DIRECTORY = 'datasets'
PATH = os.path.join(DATASETS_DIRECTORY, 'incomes', 'incomes.csv')


def generate_simplified():
    """
      The function create a new xls file with the useful data of the income file
      Return:
        The filename of the generated file
    """
    if not os.path.exists(PATH):
        path = os.path.join(DATASETS_DIRECTORY, 'incomes', 'h08.xls')
        xls = pd.ExcelFile(path)  # Import the dataframe from the Excel file
        income_raw = pd.read_excel(xls, header=4, nrows=53)  # n rows is used to import only the first table
        income_raw.drop(list(income_raw.filter(regex='Unnamed')), axis=1, inplace=True)  # Drop the unused unnamed cols
        income_raw = income_raw[pd.notna(income_raw.State)]  # Take only the rows with a not null State value

        # In the h08 dataset the key alue is the state and there are one column for each year with the corresponding
        # median income value. We are going to create a new Dataframe reshaping the previous one, the keys would be Year
        # and State as the other dataframes.

        income_frame = pd.DataFrame(columns=['Area', 'Year', 'Income'])
        years = income_raw.columns[1:]  # Save all the years in the df
        for index, row in income_raw.iterrows():  # Lets iterate through df rows (states)
            if row['State'] == 'United States':  # Rename some area names to match the key of the other datasets
                state = 'United States Total'
            elif row['State'] == 'D.C.':
                state = 'District of Columbia'
            else:
                state = row['State']
            for year in years:  # For each row a new row is added to the new df (same state and different years)
                income_frame.loc[len(income_frame)] = [state, year, row[year]]

        income_frame.to_csv(PATH)

    return PATH
