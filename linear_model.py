import seaborn as sns
import matplotlib.pyplot as plt
import math
import scipy.stats


class LinearModelCorrelation:
    """
      This class is used to work with linear models between two columns of a dataframe (also referred to years)
    """

    def __init__(self, first_col, second_col, dataframe):
        self.first_col = first_col
        self.second_col = second_col
        self.dataframe = dataframe

    def scatter_plot(self, years=[]):
        """
          Plot the models, given a set of years
          Args:
            years: A set of years
        """
        n_years = len(years)
        if n_years == 0:  # If the years array is empty, plot all te years data (for a data overview)
            plt.figure(figsize=(30, 20))
            sns.regplot(x=self.dataframe[self.first_col], y=self.dataframe[self.second_col])
        else:
            x = 0
            y = 0
            plt.clf()
            figure, axes = plt.subplots(math.ceil(n_years/2), 2, sharex=True, sharey=True, figsize=(20, 15))
            # math.ceil returns the smallest integer which is greater than or equal to the input value
            for i in range(n_years):
                df_single_y = self.dataframe[self.dataframe.Year == years[i]]
                sns.regplot(x=df_single_y[self.first_col], y=df_single_y[self.second_col],
                            ax=axes[x, y], ci=95).set_title(years[i], fontsize=20)
                if y == 0:  # Managing subplots rows and cols (always two cols)
                    y += 1
                else:
                    x += 1
                    y = 0

    def correlation(self, year):
        """
          Calculates various statistic indexes for the two columns given a year
          Args:
            year: An integer value
          Return:
            A dictionary that contains r value, standard error, R^2 and the slope of the regression line
        """
        x = self.dataframe[self.dataframe.Year == year][self.first_col]
        y = self.dataframe[self.dataframe.Year == year][self.second_col]
        slope, intercept, r_value, p_value, std_err = scipy.stats.linregress(x, y)

        return {'r': r_value, 'se': std_err, 'r2': r_value**2, 'slope': slope}
