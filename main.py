# Import libraries
import pandas as pd
import numpy as np
import os
import re
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn import linear_model
from linear_model import LinearModelCorrelation
from crime_file import CrimeFile
from income_file import generate_simplified as simplify_income_file
from folium_map import FoliumMap

# Constants: CRIME_FILES defines the filename and related years, the datasets directory path of a crime file
CRIME_FILES = [
    pd.Series({'filename': '2011.xls', 'year': 2011, 'nrows': 199}),
    pd.Series({'filename': '2012.xls', 'year': 2012, 'nrows': 200}),
    pd.Series({'filename': '2013.xls', 'year': 2013, 'nrows': 200}),
    pd.Series({'filename': '2014.xls', 'year': 2014, 'nrows': 200}),
    pd.Series({'filename': '2015.xls', 'year': 2015, 'nrows': 199}),
    pd.Series({'filename': '2016.xls', 'year': 2016, 'nrows': 199}),
    pd.Series({'filename': '2017.xls', 'year': 2017, 'nrows': 199})
]
DATASETS_DIRECTORY = 'datasets'
YEARS = [2011, 2012, 2013, 2014, 2015, 2016, 2017]

pd.options.mode.chained_assignment = None  # We've disabled this annoying warning message because there are some cases
# in which we don't care about making overwrites to the original frame.

# ------ Functions ------


def to_spaced_camel_case(snake_str):
    """
      Format a snake case string into text (ex. to make uniform column names)
      Args:
        snake_str: snake case string
      Return:
        The formatted string
    """
    components = snake_str.split('_')
    title = ''.join(x.title() for x in components)

    return re.sub("([a-z])([A-Z])", "\g<1> \g<2>", title)


def plot_dataframe_correlation(dataframe):
    """
      Plot the r correlation value for all the column combinations
      Args:
        dataframe Dataframe
    """
    plt.clf()  # Clears the plot output figure
    plt.figure(figsize=(15, 15))  # Set the figure size
    correlation_matrix = dataframe.corr()  # Generate a matrix of the correlation coefficient for all the columns
    sns.heatmap(correlation_matrix, annot=True, vmin=-1, vmax=1, square=True)  # Plot an heat map of the previous matrix
    plt.savefig('correlation.png')  # Save the matrix image


def predict_crimes_year(df, year):
    """
      Plot the forecasted total crimes rate for the given year
      Args:
        dataframe Dataframe
        int year
    """
    if year > 2017:
        predictions_df = df[df.State == 'United States Total'][['Year', 'Total Crimes', 'Population']].copy()
        prediction_years = list(range(2018, year+1))

        # Crimes rate prediction
        features = predictions_df.Year.unique().reshape(7, 1)  # We use year in order to predict the crimes
        target = predictions_df["Total Crimes"]  # Crimes are our target
        crimes_linear_model = linear_model.LinearRegression()  # Build up the linear model
        crimes_linear_model.fit(features, target)  # Fit the data to the model
        crimes_prediction = crimes_linear_model.predict(X=np.array(prediction_years).reshape(len(prediction_years), 1))
        # Reshape the years in a column (n of rows equal to list length), predict the given years' crimes

        # Population prediction
        target = predictions_df["Population"]
        pop_linear_model = linear_model.LinearRegression()
        pop_linear_model.fit(features, target)
        population_prediction = pop_linear_model.predict(X=np.array(prediction_years).reshape(len(prediction_years), 1))

        # Add predicted values to the dataframe
        for i, year in enumerate(prediction_years):
            predictions_df.loc[len(predictions_df)] = [year, crimes_prediction[i], population_prediction[i]]

        # Plot the lineplot with the forecast values
        plt.clf()
        sns.lineplot(x=predictions_df["Year"], y=predictions_df["Total Crimes"], ci=None).set_title(
            'Prediction Of Total US Crimes Rate', fontsize=20)

        # Calculate the number of crimes (prediction) and the percentage decrease (from 2017)
        # We've to use the .item() to avoid the index of the row
        prediction_year_row = predictions_df[predictions_df.Year == year]
        prediction_year_crimes = prediction_year_row['Total Crimes'].item() * prediction_year_row.Population.item()
        last_known_year_row = predictions_df[predictions_df.Year == 2017]
        last_known_year_data = last_known_year_row['Total Crimes'].item() * last_known_year_row.Population.item()
        percentage_decrease = (last_known_year_data - prediction_year_crimes)/last_known_year_data
        return {'Predicted number of crimes': int(prediction_year_crimes),
                'Predicted crime rate': prediction_year_row['Total Crimes'].item(),
                'Percentage decrease': "{:.2f}".format(percentage_decrease*100)+'%'}
    else:
        return 'The year is lower than 2017'


# ------ Main ------


crime = None
# Check if exists the simplified and ready to manipulate crimes' files, otherwise create them
for x in CRIME_FILES:
    file = CrimeFile(x.year, x.nrows, x.filename)  # Creates an instance of a CrimeFile object
    file_path = file.generate_simplified()  # Drop unnecessary data
    xls = pd.ExcelFile(file_path)
    new_frame = pd.read_excel(xls, header=0)  # Open the simplified file
    # Appending the crimes data-frames
    if crime is None:
        crime = new_frame
    else:
        crime = crime.append(new_frame, ignore_index=True, sort=False)

# Add sum of crimes column
sum_of_crimes = crime.iloc[:, 3:14].sum(axis=1)
crime['Total Crimes'] = sum_of_crimes
# Normalizing all columns (by dividing all the crimes by the state population)
crime.iloc[:, 3:15] = crime.iloc[:, 3:15].div(crime.Population, axis=0)

final_dataset = crime

# Unemployment dataset
# Importing the dataset and the two missing states data
path = os.path.join(DATASETS_DIRECTORY, 'unemployment-by-county-us', 'output.csv')
unemployment_frame = pd.read_csv(path)
other_countries = []
other_countries += [(pd.read_csv(os.path.join(DATASETS_DIRECTORY, 'unemployment-by-county-us',
                                              'Georgia.csv')), 'Georgia')]
other_countries += [(pd.read_csv(os.path.join(DATASETS_DIRECTORY, 'unemployment-by-county-us',
                                              'Florida.csv')), 'Florida')]
for country, state in other_countries:
    country['State'] = state
    simplified = country[['Year', 'State', 'unemployment rate']]
    simplified.rename(columns={"unemployment rate": "Rate"}, inplace=True)
    unemployment_frame = unemployment_frame.append(simplified, sort=True)  # Append the two missing states
# Group all the data by Year and State (taking the mean of all the months data)
unemployment_frame = unemployment_frame.groupby(['Year', 'State']).mean()
# Merge the unemployment dataframe with the previous one
final_dataset = final_dataset.merge(unemployment_frame, how='left', left_on=['Area', 'Year'],
                                    right_on=['State', 'Year'])

# Income dataset
final_path = simplify_income_file()  # Generate the reshaped income file
income_frame = pd.read_csv(final_path, index_col=[0, 1], usecols=['Area', 'Year', 'Income'])

# Merging income dataset with the final one
final_dataset = final_dataset.merge(income_frame, how='left', on=['Area', 'Year'])

# Exporting final data-frame
final_dataset.columns = [to_spaced_camel_case(x) for x in final_dataset.columns]
final_dataset.rename(columns={"Area": "State", "Rate": "Rate Of Unemployment"}, inplace=True)

path = os.path.join(DATASETS_DIRECTORY, 'merged.xls')
final_dataset.to_excel(path, sheet_name='All datasets', index=True)

path = os.path.join(DATASETS_DIRECTORY, 'merged.csv')
final_dataset.to_csv(path, index=True)


# Exporting Folium map
map_obj = FoliumMap(final_dataset, [48, -102], 'us-states.json')
map = map_obj.generate_map(['Rate Of Unemployment', 'Total Crimes', 'Income'])
map.save('Map.html')


# ------ Analysis ------
# Let's see the correlation (r) all over the columns
plot_dataframe_correlation(final_dataset[pd.notna(final_dataset['Rate Of Unemployment'])
                                         & pd.notna(final_dataset.Income)])

# ---- Rate of unemployment analysis ----
analysis_frame = final_dataset[pd.notna(final_dataset['Rate Of Unemployment'])]
# Linear model between the two
# Plot linear models
unemployment_crimes = LinearModelCorrelation('Rate Of Unemployment', 'Total Crimes', analysis_frame)
unemployment_crimes.scatter_plot()

years = [2011, 2012, 2013, 2014, 2015, 2016]
unemployment_crimes.scatter_plot(years)

print('Unemployment correlation:')
for year in years:
    print(year, unemployment_crimes.correlation(year))


# ---- Income analysis ----
analysis_frame = final_dataset[(final_dataset.State != 'United States Total') & (pd.notna(final_dataset.Income))].copy()

# Linear model between the two
# Plot linear models
income_crimes = LinearModelCorrelation('Income', 'Total Crimes', analysis_frame)
income_crimes.scatter_plot()

years = [2011, 2012, 2013, 2014, 2015, 2016]
income_crimes.scatter_plot(years)

print('Income correlation:')
for year in years:
    print(year, income_crimes.correlation(year))

# ----- Predictions -----
plt.clf()
fig = plt.figure(figsize=(10,10))
sns.lineplot(x=final_dataset["Year"], y=final_dataset[final_dataset.State == 'United States Total']["Total Crimes"],
             ci=None).set_title('Total US Crimes Rate', fontsize=20)

input_year = 0
while int(input_year) <= 2017:
    input_year = input('Insert the year to forecast total crimes rate (bigger than 2017):')
    print(predict_crimes_year(final_dataset, int(input_year)))
